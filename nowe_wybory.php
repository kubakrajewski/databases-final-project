<HTML>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
  <meta http-equiv="refresh" content="240">
<head>
 <title>Wybory do samorządu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">
<style>
body {
font-family: 'Playfair Display', sans-serif;
}
table {
  font-family: 'Playfair Display', sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

/* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
  padding-top: 15px;
  padding-bottom: 15px;
  border: 0;
  border-radius: 0;
  margin-bottom: 0;
  font-size: 12px;
  letter-spacing: 5px;
}

.text {
  font-size: large;
}

.navbar-nav li a:hover {
  color: #1abc9c !important;
}

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }
</style>
</head>
<BODY>
  <nav class="navbar navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">WYBORY DO SAMORZĄDU</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="aplikacja.html">WYLOGUJ SIĘ</a></li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container-fluid text-center">
  <div class="row content">
    <div class="col-sm-3 sidenav">
    </div>
    <div class="col-sm-6 text-left">

<h3>Zgłaszanie nowych wyborów</h3>
<div class=text>
<?PHP
$link = pg_connect("host=labdb dbname=mrbd user=jk406798 password=YGrek");
$password = $_POST["password"];
$id = $_POST["id"];
$id_komisja = $_POST["login"];
$nazwa = $_POST["nazwa"];
$data_rozp = $_POST["data_rozp"];
$data_zak = $_POST["data_zak"];
$data_zak_zgloszen = $_POST["data_zak_zgloszen"];
$liczba_stanowisk = $_POST["liczba_stanowisk"];
$ogloszone = $_POST["ogloszone"];


$wynik = pg_query($link,
                  "INSERT INTO wybory VALUES (" .
                  pg_escape_string($id) .
                  "," . pg_escape_string($id_komisja) .
                  ",'" . pg_escape_string($nazwa) .
                  "','" . pg_escape_string($data_rozp) .
                  "','" . pg_escape_string($data_zak) .
                  "','" . pg_escape_string($data_zak_zgloszen) .
                  "'," . pg_escape_string($liczba_stanowisk) .
                  ", false)");

if ($wynik) {
  echo "<text> Udało się zgłosić nowe wybory. </text><br/>";
}
else {
  echo "<text> Nie udało się zgłosić wyborów. Być może wprowadzone dane były
  niepoprawne. Pamiętaj, że id wyborów powinno być unikalną liczbą. Data rozpoczęcia
  wyborów musi być wcześniejsza niż data zakończenia, ale późniejsza niż
  data zakończenia zgłoszeń kandydatów. Daty podajemy w formacie MM/DD/RRRR GG:MM, a
  więc na przykład godzina 8:00 1 lipca 2021 będzie zapisana jako 07/01/2021 08:00.
  </text>";
}
?>
</div>
<h3>Wprowadzone dane</h3>
<table>
<tr>
        <th>id (kod) wyborów</th><th>id (numer) komisji</th>
        <th>Data rozpoczęcia</th><th>Data zakończenia</th>
        <th>Data zakończenia zgłoszeń kandydatów</th><th>Liczba stanowisk</th>
</tr>
<tr>
<td>
<?PHP
echo $id;
?>
</td>
<td>
<?PHP
echo $id_komisja;
?>
</td>
<td>
<?PHP
echo $data_rozp;
?>
</td>
<td>
<?PHP
echo $data_zak;
?>
</td>
<td>
<?PHP
echo $data_zak_zgloszen;
?>
</td>
<td>
<?PHP
echo $liczba_stanowisk;
?>
</td>
</tr>
</table>

<!--<FORM action="password.php" method=POST>
<input type=hidden name=login value=" . $login . "><br>
<input type=hidden name=password value=" . $password . "><br>
<Input TYPE=SUBMIT name="enter" value="Powrót">
</FORM> -->

</div>
</BODY>
</HTML>
