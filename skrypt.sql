CREATE TABLE Wybory (
  id_wybory int PRIMARY KEY,
  id_komisja int,
  nazwa varchar(100) NOT NULL,
  data_rozp timestamp NOT NULL,
  data_zak timestamp NOT NULL,
  data_zak_zgloszen timestamp NOT NULL,
  liczba_stanowisk int NOT NULL,
  ogloszone boolean,
  CHECK (data_zak > data_rozp),
  CHECK (data_zak_zgloszen < data_rozp)
);

CREATE TABLE Wyborcy (
  nr_ind int PRIMARY KEY,
  Imie varchar(100) NOT NULL,
  Nazwisko varchar(100) NOT NULL
);

CREATE TABLE Kandydaci (
  nr_ind int,
  id_wybory int,
  liczba_glosow int,
  UNIQUE (nr_ind, id_wybory)
);

CREATE TABLE Komisje (
  id_komisja int PRIMARY KEY
);

CREATE TABLE Glosy (
  nr_ind int,
  id_wybory int,
  UNIQUE (nr_ind, id_wybory)
);

ALTER TABLE Wybory ADD FOREIGN KEY (id_komisja) REFERENCES Komisje (id_komisja);

ALTER TABLE Kandydaci ADD FOREIGN KEY (nr_ind) REFERENCES Wyborcy (nr_ind);

ALTER TABLE Kandydaci ADD FOREIGN KEY (id_wybory) REFERENCES Wybory (id_wybory);

ALTER TABLE Glosy ADD FOREIGN KEY (nr_ind) REFERENCES Wyborcy (nr_ind);

ALTER TABLE Glosy ADD FOREIGN KEY (id_wybory) REFERENCES Wybory (id_wybory);

INSERT INTO Komisje VALUES(202101);

INSERT INTO Wyborcy VALUES (111101, 'Adam', 'Abacki');
INSERT INTO Wyborcy VALUES (111102, 'Janek', 'Pastuch');
INSERT INTO Wyborcy VALUES (111103, 'Brajan', 'Kowalewski');
INSERT INTO Wyborcy VALUES (111104, 'Bartosz', 'Babacki');
INSERT INTO Wyborcy VALUES (111105, 'Johny', 'Bravo');
INSERT INTO Wyborcy VALUES (111106, 'Arnold', 'Shwarzenegger');
INSERT INTO Wyborcy VALUES (111107, 'Arnold', 'Focault');
INSERT INTO Wyborcy VALUES (111108, 'Emma', 'Stone');
INSERT INTO Wyborcy VALUES (111109, 'Emma', 'Noether');
INSERT INTO Wyborcy VALUES (111110, 'Maria', 'Skłodowska');
INSERT INTO Wyborcy VALUES (111111, 'Zofia', 'Nałkowska');
INSERT INTO Wyborcy VALUES (111112, 'Justyna', 'Steczkowska');
INSERT INTO Wyborcy VALUES (111113, 'Zuzia', 'Borkowska');


